import unittest

from myCode import prog

class BasicTests(unittest.TestCase):

	def test_add_method(self):
		result = prog.add(33,44)
		self.assertEqual(77, result)

if __name__ == '__main__':
	unittest.main()
